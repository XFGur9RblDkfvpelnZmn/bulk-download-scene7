﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.ComponentModel;
using System.Threading;

namespace Scene7.DownloadFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            int iPerPage = 0;
            int iPage = 0;
            string sDirectory = string.Empty;

            if (args.Length == 3 && Int32.TryParse(args[0], out iPerPage) && Int32.TryParse(args[1], out iPage) && Directory.Exists(args[2]))
            {
                //Fill directory from args
                sDirectory = args[2];

                //Create client instance
                ServiceReference.IpsApiPortTypeClient client = new ServiceReference.IpsApiPortTypeClient("IpsApiSoapPort");

                //create header with auth and gzip
                ServiceReference.authHeader auth = new ServiceReference.authHeader();
                auth.user = "kees.goossens@wefashion.com";
                auth.password = "Fotos_4_Wehkamp";
                auth.gzipResponse = true;

                //create searchAssetsParams
                ServiceReference.searchAssetsParam param = new ServiceReference.searchAssetsParam();
                param.includeSubfolders = false;
                param.companyHandle = "c|4272";
                param.folder = "wefashion/";

                //define response fields and add to param
                string[] respField = new string[2];
                //respField[0] = "assetArray/items/imageInfo/optimizedPath";
                //respField[1] = "assetArray/items/imageInfo/optimizedFile";
                //respField[1] = "assetArray/items/name";
                //param.responseFieldArray = respField;

                //define assetType and add to param
                respField = new string[1];
                respField[0] = "Image";
                param.assetTypeArray = respField;

                //configure records per page and result page
                param.recordsPerPage = iPerPage;
                param.recordsPerPageSpecified = true;
                param.resultsPage = iPage;
                param.resultsPageSpecified = true;
                param.sortBy = "Name";

                StreamWriter sw = new StreamWriter("log.txt", true);
                sw.WriteLine(DateTime.Now + " : " + "Start");
                sw.Close();


                //search assets by current params
                ServiceReference.searchAssetsReturn ret = client.searchAssets(auth, param);

                //Create webclient instance
                WebClient webClient = new WebClient();

                //Loop over pics
                foreach (ServiceReference.Asset asset in ret.assetArray)
                {

                    try
                    {
                        //download file
                        webClient.DownloadFile("http://wefashion.scene7.com/is/image/" + asset.imageInfo.optimizedPath + asset.imageInfo.optimizedFile, sDirectory + "\\" + asset.name + ".tiff");
                        //System.Threading.Thread.Sleep(1000);
                    }
                    catch (WebException ex)
                    {
                        //error log
                        sw = new StreamWriter("log.txt", true);
                        sw.WriteLine(DateTime.Now + " : " + "http://wefashion.scene7.com/is/image/" + asset.imageInfo.optimizedPath + asset.imageInfo.optimizedFile + " could not be downloaded");
                        sw.Close();
                    }
                }

                //Console.WriteLine("Downloaded images {0}", param.recordsPerPage * param.resultsPage);



                sw = new StreamWriter("log.txt", true);
                sw.WriteLine(DateTime.Now + " : " + "Finish");
                sw.Close();
            }
            else
            {
                Console.WriteLine("Incorrect parameters, use as Scene7.DownloadFiles [resultsPerPage] [pageNumber] [outputDirectory]");
                Console.WriteLine("resultsPerPage and pageNumber need to be integers, outputDirectory needs to exist");
            }
        }

        // public static void isDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        // {
        //     
        // }
    }
}